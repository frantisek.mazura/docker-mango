start:
	sudo dockerd --bip=172.123.0.1/16 &> dockerd-logfile &
	sudo docker-compose up --build
	
stop:
	sudo docker-compose down
	
clean:
	sudo docker container prune
